# Fonts

[Fira Code](https://github.com/tonsky/FiraCode)

Install:

```bash
pacman -S otf-fira-code
```
