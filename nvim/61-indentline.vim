" Non-broken line

let g:indentLine_char = '▏'

" Fix bug with markdown links

let g:indentLine_setConceal = 0
