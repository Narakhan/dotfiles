" colorscheme solarized
if filereadable(expand("~/.vimrc_background"))
  let g:base16colorspace=256
  source ~/.vimrc_background
endif

set termguicolors

" Substitute non-printable colours

set list
set listchars=eol:¬,tab:>-,trail:.,nbsp:_,extends:+,precedes:+

" Turn on line numbers

set nu

" Scroll

set scrolloff=2
set sidescrolloff=5

" Gutter

set numberwidth=3
