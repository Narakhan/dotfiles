# Usage

- Files ending with `.vim` will be sourced by `init.vim`
- Lower numbers are executed first

# Install Vim-Plug

## Unix

```sh
curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```

## Windows

```powershell
md ~\AppData\Local\nvim\autoload
$uri = 'https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
(New-Object Net.WebClient).DownloadFile(
  $uri,
  $ExecutionContext.SessionState.Path.GetUnresolvedProviderPathFromPSPath(
    "~\AppData\Local\nvim\autoload\plug.vim"
  )
)
```

# Requirements

Install using the system package manager.

1. [base16-shell](https://github.com/chriskempson/base16-shell)
2. [fzf](https://github.com/junegunn/fzf)
3. [jedi](https://github.com/davidhalter/jedi)
4. [clang](https://wiki.archlinux.org/index.php/Clang)
5. [uncrustify](https://github.com/uncrustify/uncrustify)
