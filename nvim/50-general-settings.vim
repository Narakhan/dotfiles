" Force user to confirm if exiting without saving

set confirm

" Force conversion of files to utf-8 when saving

set fileencoding=utf-8

" Make backspace more consistent

set backspace=indent,eol,start

" Replaces tabs with 2 spaces

set expandtab
set tabstop=2
set shiftwidth=2
set softtabstop=2

" Use smart case in searches

set ignorecase
set smartcase

" Set language for spell check

set spelllang=en_au

" Don't remove buffers when not used

set hidden

" Persistent undo

set undofile

" Wildmenu autocompletion

set wildmode=longest:full,full

" Visual feedback while substituting

set inccommand=nosplit

" Split following reading order

set splitbelow
set splitright

" Open help in vertical splits

autocmd FileType help wincmd L
