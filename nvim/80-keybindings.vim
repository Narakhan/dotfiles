" Clear highlights on <esc>

noremap <silent><esc> :noh<CR><ESC>

" Configure leader keys to use space

let mapleader=" "
let maplocalleader=" "

" ctrl-{j,k,l,h} to move between splits

tnoremap <C-h> <C-\><C-N><C-w>h
tnoremap <C-j> <C-\><C-N><C-w>j
tnoremap <C-k> <C-\><C-N><C-w>k
tnoremap <C-l> <C-\><C-N><C-w>l
inoremap <C-h> <C-\><C-N><C-w>h
inoremap <C-j> <C-\><C-N><C-w>j
inoremap <C-k> <C-\><C-N><C-w>k
inoremap <C-l> <C-\><C-N><C-w>l
nnoremap <C-h> <C-w>h
noremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

" Center view on search results

noremap n nzz
noremap N Nzz

" Tab to switch buffers

nmap <Tab> :bnext<CR>
nmap <S-Tab> :bprevious<CR>

" Close buffer

nmap <leader>q :bd<CR>

" Maintain visual mode after shifting

vmap < <gv
vmap > >gv

" Make <ESC> exit terminal mode, and make <C-v> insert an <ESC>

tnoremap <ESC> <C-\><C-n>
tnoremap <C-v> <ESC>

" Fugitive

map <leader>d :Gvdiffsplit<CR>

" Ale

nmap <F1> <Plug>(ale_fix)

" CoC
"" Use `[g` and `]g` to navigate diagnostics

nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

"" for global rename

nmap <leader>rn <Plug>(coc-rename)

"" Jumps

nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

"" Show docs

nnoremap <silent> K :call ShowDocumentation()<CR>

"" Start autocompetions

inoremap <silent><expr> <c-space> coc#refresh()

" Fuzzy find

noremap ;b :Buffers<CR>
noremap ;c :Commits<CR>
noremap ;f :Files<CR>
noremap ;m :Marks<CR>
noremap ;t :Tags<CR>
noremap ;g :Rg<CR>
noremap ;w :Windows<CR>

" quick navigation
map <Leader>l <Plug>(easymotion-lineforward)
map <Leader>j <Plug>(easymotion-j)
map <Leader>k <Plug>(easymotion-k)
map <Leader>h <Plug>(easymotion-linebackward)
