" Eyecandy

let g:ale_sign_error =   '⛔'
let g:ale_sign_warning = '⚡'

highlight link ALEErrorSign DiffDelete

" Enable fixers (defaults under *)

let g:ale_fixers = {
\   '*': ['remove_trailing_lines', 'trim_whitespace', 'prettier'],
\   'python': ['add_blank_lines_for_python_control_statements', 'isort', 'black'],
\   'c': ['clang-format'],
\   'c++': ['clang-format'],
\   'markdown': ['prettier'],
\   'xml': ['xmllint'],
\   'rust': ['rustfmt']
\}

" Linters

let g:ale_lint_on_text_changed = 'always'

"" Work structures projects weirdly
let g:ale_python_flake8_change_directory = 'off'

" Show errors when cursor near

let g:ale_virtualtext_cursor = 1
