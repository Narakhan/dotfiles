" Source all *.vim files

for f in glob("$XDG_CONFIG_HOME/nvim/*.vim", 0, 1)
  if g:f !~ 'init.vim'
    execute 'source' f
  endif
endfor
