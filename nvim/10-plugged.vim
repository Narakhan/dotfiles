call plug#begin('~/.local/share/nvim/plugged')

" Eyecandy

Plug 'ryanoasis/vim-devicons'
Plug 'chriskempson/base16-vim'
Plug 'luochen1990/rainbow'

" auto completion, linting, fixing

Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'w0rp/ale'

" Fuzzy finder

Plug 'junegunn/fzf.vim'

" Syntax

Plug 'sheerun/vim-polyglot'
Plug 'numirias/semshi', {'do': ':UpdateRemotePlugins'}

" Visual

Plug 'Yggdroot/indentLine'
Plug 'google/vim-searchindex'

" tpope

Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-unimpaired'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-eunuch'
Plug 'tpope/vim-commentary'

" Provides more text objects

Plug 'wellle/targets.vim'

" Motions

Plug 'easymotion/vim-easymotion'

" Status bar

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

call plug#end()
