####
# HISTORY
####

setopt BANG_HIST                 # Treat the '!' character specially during expansion.
setopt EXTENDED_HISTORY          # Write the history file in the ":start:elapsed;command" format.
setopt INC_APPEND_HISTORY        # Write to the history file immediately, not when the shell exits.
setopt SHARE_HISTORY             # Share history between all sessions.
setopt HIST_EXPIRE_DUPS_FIRST    # Expire duplicate entries first when trimming history.
setopt HIST_REDUCE_BLANKS        # Remove superfluous blanks before recording entry.
setopt HIST_VERIFY               # Don't execute immediately upon history expansion.
setopt HIST_IGNORE_SPACE         # Don't add entry to history if first character is a space.

setopt promptsubst

setopt alwaystoend
setopt autocd
setopt autopushd
setopt completeinword
setopt noflowcontrol
setopt histignoredups
setopt interactivecomments
setopt longlistjobs
setopt pushdignoredups
setopt pushdminus
