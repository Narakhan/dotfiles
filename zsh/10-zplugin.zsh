### Added by Zplugin's installer

source '/home/narakhan/.config/zsh/.zplugin/bin/zplugin.zsh'
autoload -Uz _zplugin
(( ${+_comps} )) && _comps[zplugin]=_zplugin

### End of Zplugin's installer chunk

#####################
# oh-my-zsh
#####################

zplugin snippet "OMZ::lib/completion.zsh"
zplugin snippet "OMZ::lib/git.zsh"
zplugin snippet "OMZ::lib/key-bindings.zsh"
zplugin snippet "OMZ::lib/prompt_info_functions.zsh"
zplugin snippet "OMZ::lib/theme-and-appearance.zsh"

# Agnoster theme

zplugin snippet "OMZ::themes/agnoster.zsh-theme"

#####################
# Load Plugins
#####################

# Double tap <CTRL-Z>

zplugin load "mdumitru/fancy-ctrl-z"

# Base16 colours

zplugin load "chriskempson/base16-shell"

# Syntax highlighting for zsh

zplugin load "zdharma/fast-syntax-highlighting"

# Conda completions

zplugin load "esc/conda-zsh-completion"

####################
# Load Completions
####################

autoload -Uz compinit
compinit

zplugin cdreplay -q
