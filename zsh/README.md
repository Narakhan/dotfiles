# Usage

- Files ending with `.zsh` will be sourced by `.zshrc`
- Lower numbers are executed first

# Exports

- `00-exports.zsh` exports all variables found in `environment.d`

**Note:** `00-exports.zsh` does not noticeably increase start-up times

# Requirements

Install using the system package manager.

1. [zplugin](https://github.com/zdharma/zplugin)
2. [fzf](https://github.com/junegunn/fzf)
