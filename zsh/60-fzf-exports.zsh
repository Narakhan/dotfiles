export FZF_DEFAULT_COMMAND='rg --files --hidden --follow --smart-case --no-messages --glob "!.git/*"'

export FZF_CTRL_T_COMMAND=$FZF_DEFAULT_COMMAND
export FZF_ALT_C_COMMAND='fd --type d --hidden --follow --exclude .git'
