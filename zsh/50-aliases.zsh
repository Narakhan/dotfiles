##############################
# Override default commands
##############################

# More readable ls
#
alias ls="exa"
alias l="exa -lag --git"
alias tree="exa --tree"

alias cat='bat'

# Config files for x

alias startx="startx $XDG_CONFIG_HOME/x/xinitrc"

# Colour in tree

# alias tree='tree -C'

# Mail sync clients

alias mbsync="mbsync -c $XDG_CONFIG_HOME/mbsync/mbsyncrc"

# More sensible defaults for lsblk

alias lsblk="lsblk -o name,size,type,fstype,label,mountpoint"

# msmtp

alias msmtp="msmtp --file=$XDG_CONFIG_HOME/msmtp/msmtprc"

# wget

alias wget="wget --hsts-file=\"$XDG_CACHE_HOME/wget-hsts\""

# neomutt fix colours

alias neomutt="neomutt; zsh $HOME/.base16_theme"

# bare '-' pops directories

alias -- -="cd -"

# history has same behaviour as bash

alias history='fc -l 1'

# Add highlighting to grep

alias grep='grep --color'

#######################
# Add new commands
#######################

# Neovim as vim

alias vim="nvim"

# Quickly blank the screen

alias blank='xset dpms force off'

# Copy stdin to system clipboard

alias copy="xclip -sel c -r"

# perl-rename

alias rename="perl-rename"

# wpa_supplicant cli

alias wpa="wpa_cli -i wlan"
