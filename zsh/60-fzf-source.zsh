# Sources fzf scripts

if [[ -e /usr/share/fzf/ ]]; then
  for file in /usr/share/fzf/*.zsh; do
    source $file
  done
fi
