# systemd environmental variables need to be expanded

for file in ${XDG_CONFIG_HOME}/environment.d/*.conf; do
  for line in $(cat $file); do
    export `eval "echo $line"`
  done
done
