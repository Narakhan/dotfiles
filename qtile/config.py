from libqtile import hook, layout
from libqtile.command import lazy

# from libqtile.lazy import lazy
from libqtile.config import Click, Drag, Group, Key

mod = "mod4"

keys = [
    # Spawn terminal
    Key([mod, "shift"], "Return", lazy.spawn("kitty")),
    # Spawn dmenu
    Key([mod], "p", lazy.spawn("dmenu_run")),
    # Spawn pass
    Key([mod, "control"], "p", lazy.spawn("passmenu")),
    # Screen capture
    Key([mod], "c", lazy.spawn("scrot ~/downloads/screen_capture_%Y%m%d%H%M%S.png -s")),
    # Blank screen
    Key([], "Scroll_Lock", lazy.spawn("xset dpms force off")),
    # Switch between windows in current stack pane
    Key([mod], "j", lazy.layout.next()),
    Key([mod], "k", lazy.layout.previous()),
    # Grow/shrink windows
    Key([mod], "h", lazy.layout.grow()),
    Key([mod], "l", lazy.layout.shrink()),
    # Rotate through available layouts
    Key([mod], "space", lazy.next_layout()),
    # Move windows up or down in current stack
    Key([mod, "shift"], "j", lazy.layout.shuffle_down()),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up()),
    Key([mod], "Tab", lazy.screen.toggle_group()),
    # Media keys
    Key([], "XF86AudioLowerVolume", lazy.spawn("amixer set Master 5%-")),
    Key([], "XF86AudioMute", lazy.spawn("amixer set Master toggle")),
    Key([], "XF86AudioRaiseVolume", lazy.spawn("amixer set Master 5%+")),
    Key([], "XF86AudioPrev", lazy.spawn("playerctl previous")),
    Key([], "XF86AudioPlay", lazy.spawn("playerctl play-pause")),
    Key([], "XF86AudioPause", lazy.spawn("playerctl play-pause")),
    Key([], "XF86AudioNext", lazy.spawn("playerctl next")),
    # Kill current window
    Key([mod, "shift"], "c", lazy.window.kill()),
    # Restart qtile
    Key([mod], "q", lazy.restart()),
    # Kill qtile
    Key([mod, "shift"], "q", lazy.shutdown()),
    # Toggle Float
    Key([mod], "t", lazy.window.toggle_floating()),
    # Change focus
    Key([mod], "a", lazy.to_screen(0)),
    Key([mod], "s", lazy.to_screen(1)),
    Key([mod], "d", lazy.to_screen(2)),
]

groups = [Group(str(i)) for i in [*range(1, 10), 0]]

for i in groups:
    keys.extend(
        [
            # mod1 + letter of group = switch to group
            Key([mod], i.name, lazy.group[i.name].toscreen()),
            # mod1 + shift + letter of group = switch to & move focused window to group
            Key([mod, "shift"], i.name, lazy.window.togroup(i.name)),
        ]
    )

layouts = [
    layout.MonadTall(border_width=1, single_border_width=0),
    layout.Max(),
    layout.Matrix(border_focus="#ff0000"),
]


screens = []

# Drag floating layouts.
mouse = [
    Drag(
        [mod],
        "Button1",
        lazy.window.set_position_floating(),
        start=lazy.window.get_position(),
    ),
    Drag(
        [mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()
    ),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

floating_layout = layout.Floating(
    float_rules=[
        {"wmclass": "confirm"},
        {"wmclass": "dialog"},
        {"wmclass": "download"},
        {"wmclass": "error"},
        {"wmclass": "file_progress"},
        {"wmclass": "notification"},
        {"wmclass": "splash"},
        {"wmclass": "toolbar"},
        {"wmclass": "confirmreset"},  # gitk
        {"wmclass": "makebranch"},  # gitk
        {"wmclass": "maketag"},  # gitk
        {"wname": "branchdialog"},  # gitk
        {"wname": "pinentry"},  # GPG key password entry
        {"wmclass": "ssh-askpass"},  # ssh-askpass
    ]
)


@hook.subscribe.client_new
def floating_dialogs(window):
    dialog = window.window.get_wm_type() == "dialog"
    popup = window.window.get_wm_type() == "popup"
    transient = window.window.get_wm_transient_for()

    if (dialog or transient) or popup:
        window.floating = True


auto_fullscreen = True
focus_on_window_activation = "smart"
dgroups_key_binder = None
dgroups_app_rules = []
main = None
follow_mouse_focus = False
bring_front_click = False
cursor_warp = False

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, github issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
